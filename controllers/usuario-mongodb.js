const MongoClient = require('mongodb').MongoClient

//const client = new MongoClient("mongodb+srv://dbHannah:8M0qG0S1w0XuMD3k@hannah.l7ie3.mongodb.net/test?authSource=dbhannah&replicaSet=atlas-f87ud8-shard-0&readPreference=primary&appname=MongoDB%20Compass&retryWrites=true&ssl=true");


var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
var config = require('../security/config');

const promisify = fn => {
        return (...args) => {
            return new Promise((resolve, reject) => {
                function customCallback(err, ...results) {
                    if (err) {
                        return reject(err);
                    }
                    return resolve(results.length === 1 ? results[0] : results)
                }

                args.push(customCallback);
                fn.call(this, ...args);
            })
        }
    }
    // Connecting to MongoDB


var db
const uri = "mongodb+srv://dbHannah:8M0qG0S1w0XuMD3k@hannah.l7ie3.mongodb.net/test";

MongoClient.connect(uri, (err, database) => {
    if (err)
        return console.log(err)
    db = database.db("dbhannah")
})

module.exports = {
    create(req, res) {
        var hashedPassword = bcrypt.hashSync(req.body.password, 8);
        //let userData = req.body
        let newUserListing = {
            email: req.body.email,
            password: hashedPassword
        };
        const result = db
            .collection("users")
            .insertOne(newUserListing);
        console.log(`Nuevo listado creado con la siguiente identificación id: ${result.insertedId}`);

        var token = jwt.sign({ id: result.insertedId }, config.secret, {
            expiresIn: 86400 // expires in 24 hours
        });
        console.log(token)
        res.status(200).send({ auth: true, token: token });
        return true;

    },
    logout(req, res) {
        res.status(200).send({ auth: false, token: null });
    },
    auth(req, res) {
        let body = {
            email: req.body.email
        };
        db.collection("users")
            .findOne(body)
            .then(user => {
                var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
                if (!passwordIsValid) return res.status(401).send({ auth: false, token: null });

                let payload = { subject: user._id }
                var token = jwt.sign(payload, config.secret, {
                    expiresIn: 86400 // expires in 24 hours
                });

                res.status(200).send({ auth: true, token: token });
            })
        return true;
    },
    listSpecials(req, res) {
        const result = db
            .collection("messages")
            .find().toArray((err, result) => {
                if (err) return console.log(err)
                    // renders index.ejs
                res.json(result);
            })
        return true;
    }
};