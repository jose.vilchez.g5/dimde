'use strict';
module.exports = (sequelize, DataTypes) => {
    const users = sequelize.define('users', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER
        },
        lastname: {
            allowNull: true,
            type: DataTypes.STRING
        },
        name: {
            allowNull: true,
            type: DataTypes.STRING
        },
        email: {
            allowNull: true,
            type: DataTypes.STRING,
            unique:true
        },
        password: {
            allowNull: true,
            type: DataTypes.STRING
        },
        idCard: {
            allowNull: true,
            type: DataTypes.STRING
        },
        
        address: {
            allowNull: true,
            type: DataTypes.STRING
        },
        status: {
            allowNull: true,
            type: DataTypes.INTEGER
        },
        roles: {
            allowNull: true,
            type: DataTypes.TEXT
        },

        photoProfile: {
            allowNull: true,
            type: DataTypes.STRING
        }
    }, {
        timestamps: true,
        freezeTableName: true,
        tableName: 'users',
        classMethods: {}
    });
    users.associate = function(models) {
        // associations can be defined here
    };
    return users;
};