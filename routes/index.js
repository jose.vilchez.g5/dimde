/* Controllers */
const usuarioController = require('../controllers/usuario');

var VerifyToken = require('../security/VerifyToken');

module.exports = (app) => {

    app.post('/api/auth/register', usuarioController.create);

    app.get('/api/auth/user', VerifyToken, usuarioController.findUserByToken);

    
    app.post('/api/auth/login', usuarioController.auth);
    app.post('/api/auth/logout', usuarioController.logout);

    app.post('/api/auth/all', VerifyToken, usuarioController.list);

    app.post('/api/auth/find', VerifyToken, usuarioController.find);

    app.post('/api/auth/activate', VerifyToken, usuarioController.activate);

    //app.get('/api/Mongodb/auth/user', VerifyToken, usuarioController.findUserByToken);
   
};